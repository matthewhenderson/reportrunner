﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;

namespace ReportRunner
{
    /// <summary>
    /// Handles setting up an email with attachments
    /// </summary>
    public class Emailer : IDisposable
    {
        private List<Attachment> _attachements = new List<Attachment>();
        private List<string> _addresses;
        private List<string> _ccAddresses;
        private List<string> _bCCAddresses;
        private Settings _settings;

        public Emailer(Settings settings)
        {
            _settings = settings;
            _addresses = settings.ToAddresses;
            _ccAddresses = settings.CCAddresses;
            _bCCAddresses = settings.BCCAddresses;
        }

        /// <summary>
        /// Adds a file as an attachment
        /// </summary>
        /// <param name="attachment">The file as a stream</param>
        /// <param name="fileName">The name of the file</param>
        public void AddAttachment(Stream attachment, string fileName)
        {
            var attach = new Attachment(attachment, fileName);
            _attachements.Add(attach);
        }

        /// <summary>
        /// Tries to send an email
        /// </summary>
        public void Send()
        {
            //don't bother sending a message if there are no attachments
            if(!_attachements.Any())
                return;

            var msg = new MailMessage();
            AddAttachments(msg);
            SetupAddresses(msg);

            msg.From = new MailAddress(_settings.FromAddress);
            msg.Subject = _settings.Subject;
            msg.Body = _settings.Body;
            msg.IsBodyHtml = true;

            var client = new SmtpClient
            {
                UseDefaultCredentials = false,
                Port = _settings.Port,
                Host = _settings.SMTPHost,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                EnableSsl = _settings.EnableSsl
            };

            if (_settings.UseLogin())
            {
                client.Credentials = new System.Net.NetworkCredential(_settings.EmailUser, _settings.EmailPassword);
            }

            try
            {
                client.Send(msg);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        /// <summary>
        /// Adds all TO addresses
        /// </summary>
        /// <param name="message">The message to add to</param>
        private void SetupAddresses(MailMessage message)
        {
            foreach (string address in _addresses.Where(x => !String.IsNullOrWhiteSpace(x)))
            {
                message.To.Add(new MailAddress(address));
            }

            foreach (string cc in _ccAddresses.Where(x => !String.IsNullOrWhiteSpace(x)))
            {
                message.CC.Add(new MailAddress(cc));
            }

            foreach (string bcc in _bCCAddresses.Where(x => !String.IsNullOrWhiteSpace(x)))
            {
                message.Bcc.Add(new MailAddress(bcc));
            }
        }

        /// <summary>
        /// Adds all attachments to the message
        /// </summary>
        /// <param name="message">The message to add to</param>
        private void AddAttachments(MailMessage message)
        {
            foreach (Attachment attachement in _attachements)
            {
                message.Attachments.Add(attachement);
            }
        }

        /// <summary>
        /// Need to clean up streams
        /// </summary>
        public void Dispose()
        {
            foreach (Attachment attachement in _attachements)
            {
                attachement.Dispose();
            }
        }
    }
}
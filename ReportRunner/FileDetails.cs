﻿using System;

namespace ReportRunner
{
    /// <summary>
    /// For keeping track of report file details
    /// </summary>
    public class FileDetails
    {
        public FileDetails(string name, string path)
        {
            FileName = name;
            FilePath = path;
            SaveAs = name.Replace(".rpt", "");//removing report extension
        }

        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string SaveAs { get; set; }
    }
}

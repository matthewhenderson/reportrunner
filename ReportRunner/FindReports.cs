﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace ReportRunner
{
    /// <summary>
    /// Finds all crystal reports in the same directory as the program
    /// </summary>
    public class FindReports
    {
        private string _currentPath;

        public FindReports()
        {
            _currentPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
        }

        /// <summary>
        /// Finds all reports
        /// </summary>
        /// <returns>A list of report paths</returns>
        public List<FileDetails> GetReportFileNames()
        {
            var d = new DirectoryInfo(_currentPath);
            var files = d.GetFiles("*.rpt");
            var reports = new List<FileDetails>();
            foreach (FileInfo file in files)
            {
                reports.Add(new FileDetails(file.Name, _currentPath + "\\" + file.Name));
            }

            return reports;
        }
    }
}
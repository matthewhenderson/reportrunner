﻿using System;
using System.IO;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace ReportRunner
{
    /// <summary>
    /// Loads a crystal report for export
    /// </summary>
    public class ReportHandler
    {
        private ReportDocument _reportDocument;

        public ReportHandler(string reportPath)
        {
            _reportDocument = new ReportDocument();
            _reportDocument.Load(reportPath);
        }

        /// <summary>
        /// True if the report has any data
        /// </summary>
        public bool HasData
        {
            get { return _reportDocument.HasRecords; }
        }

        /// <summary>
        /// Gets the crystal report data as an excel memory stream
        /// </summary>
        /// <returns>A memory stream</returns>
        public Stream GetExportStream()
        {
            try
            {
                return _reportDocument.ExportToStream(ExportFormatType.Excel);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }
    }
}
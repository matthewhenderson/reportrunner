﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace ReportRunner
{
    public class Settings
    {
        public Settings ()
        {
            FromAddress = ConfigurationManager.AppSettings["from"];
            ToAddresses = ConfigurationManager.AppSettings["to"].Split(',').ToList();
            CCAddresses = ConfigurationManager.AppSettings["cc"].Split(',').ToList();
            BCCAddresses = ConfigurationManager.AppSettings["bcc"].Split(',').ToList();
            Subject = ConfigurationManager.AppSettings["subject"];
            Body = ConfigurationManager.AppSettings["body"];
            SMTPHost = ConfigurationManager.AppSettings["SMTPHost"];
            Port = Int32.Parse(ConfigurationManager.AppSettings["Port"]);
            EmailUser = ConfigurationManager.AppSettings["emailuser"];
            EmailPassword = ConfigurationManager.AppSettings["emailpassword"];
            EnableSsl = Boolean.Parse(ConfigurationManager.AppSettings["ssl"]);
        }

        public string FromAddress { get; set; }
        public List<string> ToAddresses { get; set; }
        public List<string> CCAddresses { get; set; }
        public List<string> BCCAddresses { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string SMTPHost { get; set; }
        public int Port { get; set; }
        public string EmailUser { get; set; }
        public string EmailPassword { get; set; }
        public bool EnableSsl { get; set; }

        public bool UseLogin()
        {
            return !String.IsNullOrWhiteSpace(EmailUser) && !String.IsNullOrWhiteSpace(EmailPassword);
        }
    }
}
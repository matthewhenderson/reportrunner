﻿using System;

namespace ReportRunner
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                using (var emailer = new Emailer(new Settings()))
                {

                    var files = new FindReports();
                    foreach (FileDetails file in files.GetReportFileNames())
                    {
                        var reportHandler = new ReportHandler(file.FilePath);
                        //skip report if there is no data
                        if(!reportHandler.HasData)
                            continue;

                        //streams need to stay open until message is sent
                        var stream = reportHandler.GetExportStream();
                        
                        //only add attachment if there was content
                        if (stream.Length > 0)
                        {
                            emailer.AddAttachment(stream, String.Format("{0}.xls", file.SaveAs));
                        }
                        else
                        {
                            stream.Dispose();
                        }
                    }

                    emailer.Send();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}